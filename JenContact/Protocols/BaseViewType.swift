//
//  BaseViewType.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import UIKit

protocol BaseViewType {
  func showAlertWith(title: String, message: String)
}

extension BaseViewType {
  func showLoading() {
    UIAlertController(title: nil, message: "Loading ...", preferredStyle: .alert).show()
  }
  
  func showAlertWith(title: String, message: String) {
    show(title: title, message: message)
  }
  
  private func show(title: String?, message: String?) {
    let  alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Oke", style: .default, handler: nil))
    alert.show()
  }
}

