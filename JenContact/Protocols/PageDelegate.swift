//
//  PageDelegate.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import Foundation

protocol PageDelegate: class {
  func setupPage(with state: PageState)
}

extension PageDelegate {
  func setupPage(with state: PageState) {}
}

