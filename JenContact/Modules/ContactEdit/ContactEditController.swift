//
//  ContactEditController.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import UIKit

class ContactEditController: UITableViewController, BaseViewType {
  
  // MARK: - Coordinator Functions
  var onCancelPressed: (() -> Void)?
  var onDonePressed: (() -> Void)?
  
  // MARK: - Properties
  private lazy var headerView: ContactHeaderView = {
    let header = ContactHeaderView()
    return header
  }()
  
  var viewModel: ContactDetailViewModel!
  var pageType: ContactDetailType = .view
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupNavigation()
    setupTableView()
    configureHeader()
    registerKeyboardNotifications()
    /// Page delegate
    viewModel.delegate = self
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  private func setupNavigation() {
    navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancel(_:)))
    navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(handleDone(_:)))
    navigationController?.navigationBar.configure(with: .transparent(translucent: true))
  }
  
  private func setupTableView() {
    tableView.registerReusableCell(ContactInfoCell.self)
    tableView.delegate = self
    tableView.dataSource = self
    tableView.separatorColor = UIColor.groupTableViewBackground
    tableView.tableFooterView = UIView()
    tableView.tableHeaderView = headerView
    tableView.allowsSelection = false
  }
  
  private func registerKeyboardNotifications() {
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(keyboardWillShow(notification:)),
                                           name: UIResponder.keyboardWillShowNotification,
                                           object: nil)
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(keyboardWillHide(notification:)),
                                           name: UIResponder.keyboardWillHideNotification,
                                           object: nil)
  }
  
  @objc
  private func keyboardWillShow(notification: NSNotification) {
    let userInfo: NSDictionary = notification.userInfo! as NSDictionary
    let keyboardInfo = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
    let keyboardSize = keyboardInfo.cgRectValue.size
    var contentInsets = tableView.contentInset
    contentInsets.bottom = keyboardSize.height - ContactHeaderView.headerHeight
    tableView.contentInset = contentInsets
    tableView.scrollIndicatorInsets = contentInsets
  }
  
  @objc
  private func keyboardWillHide(notification: NSNotification) {
    var contentInsets = tableView.contentInset
    contentInsets.bottom = 0
    tableView.contentInset = contentInsets
    tableView.scrollIndicatorInsets = contentInsets
  }
  
  private func configureHeader() {
    headerView.configureView(with: .edit, viewModel: self.viewModel)
  }
  
  @objc
  private func handleCancel(_ sender: UIBarButtonItem) {
    onCancelPressed?()
  }
  
  @objc
  private func handleDone(_ sender: UIBarButtonItem) {
    viewModel.updateContact()
  }
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return self.viewModel.numberOfItemsInSection()
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.viewModel.numberOfRowsInSection(section: section)
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(indexPath: indexPath) as ContactInfoCell
    cell.configureCell(item: viewModel.items(cellForRowAt: indexPath), type: pageType)
    cell.textFieldInfo.delegate = self
    cell.textFieldInfo.addTarget(self, action: #selector(textfieldChanged(_:)), for: .editingDidEnd)
    cell.textFieldInfo.tag = indexPath.row
    return cell
  }
  
  @objc
  private func textfieldChanged(_ sender: UITextField) {
    let type = viewModel.items(cellForRowAt: IndexPath(row: sender.tag, section: 0))
    switch type {
    case .firstName:
      viewModel.setFirstName(with: sender.text)
    case .lastName:
      viewModel.setLastName(with: sender.text)
    case .age:
      viewModel.setAge(with: sender.text)
    case .email:
      viewModel.setEmail(with: sender.text)
    default: break
    }
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return ContactInfoCell.height
  }
}

extension ContactEditController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.endEditing(true)
    return true
  }
}

extension ContactEditController: PageDelegate {
  func setupPage(with state: PageState) {
    switch state {
    case .success:
      DispatchQueue.main.async {
        self.tableView.reloadData()
      }
    case .empty:
      /// After successfully updated show alert
      let alert = UIAlertController(title: nil, message: "Successfully update Contact", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self] _ in
        self?.onDonePressed?()
      }))
      alert.show()
    case .error(let error):
      showAlertWith(title: "Error", message: error)
    case .routeBack:
      let alert = UIAlertController(title: nil, message: "Successfully create Contact", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self] _ in
        self?.navigationController?.popViewController(animated: true)
      }))
      alert.show()
    case .loading:
      break
    }
  }
}
