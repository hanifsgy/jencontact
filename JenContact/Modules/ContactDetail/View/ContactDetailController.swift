//
//  ContactDetailController.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import Foundation
import UIKit

class ContactDetailController: UITableViewController, BaseViewType {
  
  // MARK: - Coordinator Functions
  var onBackPressed: (() -> Void)?
  var onEditPressed: (() -> Void)?
  
  // MARK: - Properties
  var viewModel: ContactDetailViewModel!
  var pageType: ContactDetailType = .view
    
  private lazy var headerView: ContactHeaderView = {
    let header = ContactHeaderView()
    header.delegate = self
    return header
  }()
  
  private lazy var activityIndicator: UIActivityIndicatorView = {
    let activity = UIActivityIndicatorView(style: .medium)
    return activity
  }()
  // MARK: - ViewDidLoad
  override func viewDidLoad() {
    super.viewDidLoad()
    setupNavigation()
    setupTableView()
    setupHeader()
    setupActivityIndicator()
    /// Page delegate
    viewModel.delegate = self
  }
  // MARK: - View Will Appear
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
//    viewModel.fetchContact()
  }
  
  // MARK: - View Will Dissappear
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    navigationController?.navigationBar.configure(with: .solid(color: UIColor.white))
  }
  
  private func setupNavigation() {
    navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(handleEdit(_:)))
    navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleClose(_:)))
    navigationController?.navigationBar.configure(with: .transparent(translucent: true))
  }
  
  private func setupTableView() {
    tableView.registerReusableCell(ContactInfoCell.self)
    tableView.delegate = self
    tableView.dataSource = self
    tableView.tableHeaderView = headerView
    tableView.tableFooterView = UIView()
    tableView.separatorColor = UIColor.systemGroupedBackground
  }
  
  private func setupHeader() {
    headerView.configureView(with: pageType, viewModel: self.viewModel)
  }
  
  private func setupActivityIndicator() {
    view.addSubview(activityIndicator)
    let bounds = view.bounds
    activityIndicator.frame.origin = CGPoint(x: (bounds.width - activityIndicator.frame.width) / 2.0, y: (bounds.height - activityIndicator.frame.height) / 2.0)
  }
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return viewModel.numberOfItemsInSection()
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return ContactInfoCell.height
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRowsInSection(section: section)
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(indexPath: indexPath) as ContactInfoCell
    cell.configureCell(item: viewModel.items(cellForRowAt: indexPath), type: pageType)
    return cell
  }
    
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    // in case delete contact
    if indexPath.section == 1 && indexPath.row == 0 {
      let alert = UIAlertController(title: nil, message: "Do you want to delete this contact?", preferredStyle: .alert)
       alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [weak self] _ in
          self?.viewModel.deleteContact()
       }))
       alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
       alert.show()
    }
  }
  
  @objc
  private func handleEdit(_ sender: UIBarButtonItem) {
    onEditPressed?()
  }
  @objc
  private func handleClose(_ sender: UIBarButtonItem) {
    onBackPressed?()
  }
}

extension ContactDetailController: PageDelegate {
  func setupPage(with state: PageState) {
    switch state {
    case .loading:
      DispatchQueue.main.async {
        self.activityIndicator.startAnimating()
      }
    case .success:
      DispatchQueue.main.async {
        self.tableView.reloadData()
        self.tableView.tableHeaderView?.reloadInputViews()
        self.activityIndicator.stopAnimating()
      }
    case .error(let error):
      self.activityIndicator.stopAnimating()
      showAlertWith(title: "Error", message: error)
    case .routeBack:
      self.navigationController?.popViewController(animated: true)
    case .empty:
      break
    }
  }
}

extension ContactDetailController: ContactHeaderDelegate {
  func favouriteAction(_ view: ContactHeaderView, action: Bool) {
    let alert = UIAlertController(title: nil, message: action ? "Favourite this Contact?" : "Unfavourite this Contact?", preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [weak self] _ in
      view.btnFavourite.isSelected = action
      self?.viewModel.updateFavourite(action: action)
    }))
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    alert.show()
  }
}
