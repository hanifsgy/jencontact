//
//  ContactInfoCell.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import UIKit

class ContactInfoCell: UITableViewCell {
  
  @IBOutlet weak var lblInfo: UILabel!
  @IBOutlet weak var textFieldInfo: UITextField!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
  }
}

extension ContactInfoCell: IReusableCell {
  static var height: CGFloat {
    return 56.0
  }
  
  func configureCell(item: ContactDetailMenuType, type: ContactDetailType) {
    self.lblInfo.text = item.description
    self.textFieldInfo.text = item.content
    
    if item.content == nil {
      self.lblInfo.textColor = UIColor.red
    }
    switch type {
    case .edit, .new:
      textFieldInfo.isEnabled = true
    default:
      textFieldInfo.isEnabled = false
    }
  
    self.textFieldInfo.keyboardType = item.keyboardType
  }
}
