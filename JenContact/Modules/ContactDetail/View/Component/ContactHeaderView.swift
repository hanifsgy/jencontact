//
//  ContactHeaderView.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import UIKit
import Kingfisher

protocol ContactHeaderDelegate: class {
  func favouriteAction(_ view: ContactHeaderView, action: Bool)
}

class ContactHeaderView: UIView {
  
  @IBOutlet weak var container: UIView!
  @IBOutlet weak var contactPhoto: CircularUImage!
  @IBOutlet weak var contactFullname: UILabel!
  @IBOutlet weak var btnFavourite: UIButton!
  @IBOutlet weak var btnEditPhoto: UIButton!
  @IBOutlet weak var containerStackBottom: UIView!
  
  weak var delegate: ContactHeaderDelegate?
  
  override init(frame: CGRect) {
    let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 314)
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  private func setup() {
    let view = loadNib()
    view.frame = bounds
    view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    addSubview(view)
    
    container.applyGradient(colors: [UIColor.white.cgColor, Color.primary_blue.cgColor])
    contactPhoto.kf.indicatorType = .activity
    btnFavourite.addTarget(self, action: #selector(handleFavourite(_:)), for: .touchUpInside)
  }
  
  @objc
  private func handleFavourite(_ sender: UIButton) {
    if btnFavourite.isSelected == true {
      delegate?.favouriteAction(self, action: false)
    } else {
      delegate?.favouriteAction(self, action: true)
    }
  }
}

extension ContactHeaderView {
  
  func configureView(with page: ContactDetailType, viewModel: ContactDetailViewModel) {
    switch page {
    case .edit, .new:
      containerStackBottom.isHidden = true
      let model = viewModel.getContact()
      guard let name = model?.fullName, let profilePic = model?.photo else { return }
      if profilePic == "N/A" {
        contactPhoto.image = UIImage(named: "placeholder_photo")
      } else {
        contactPhoto.kf.setImage(with: URL(string: profilePic), placeholder: UIImage(named: "placeholder_photo"))
      }
      contactFullname.text = name
    case .view:
      btnEditPhoto.isHidden = true
      let model = viewModel.getContact()
      guard let name = model?.fullName, let profilePic = model?.photo    else { return }
      if profilePic == "N/A" {
          contactPhoto.image = UIImage(named: "placeholder_photo")
      } else {
        contactPhoto.kf.setImage(with: URL(string: profilePic), placeholder: UIImage(named: "placeholder_photo"))
      }
      contactFullname.text = name
      btnFavourite.isSelected = model?.favorite ?? false
    }
  }
  
  static var headerHeight: CGFloat {
    return 314
  }
}
