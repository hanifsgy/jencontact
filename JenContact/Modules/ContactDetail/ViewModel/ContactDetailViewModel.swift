//
//  ContactDetailViewModel.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import Foundation

protocol ContactDetailViewModelType {
  func numberOfItemsInSection() -> Int
  func numberOfRowsInSection(section: Int) -> Int
  func items(cellForRowAt indexPath: IndexPath) -> ContactDetailMenuType
  func getContact() -> Contact?
  func deleteContact()
  
  func updateContact()
  func updateFavourite(action: Bool)
  
  func setFirstName(with value: String?)
  func setLastName(with value: String?)
  func setPhoneNumber(with value: String?)
  func setEmail(with value: String?)
  func setAge(with value: String?)
  func setPhoto(with value: String)
}

final class ContactDetailViewModel: ContactDetailViewModelType {
  weak var delegate: PageDelegate?
  internal var items: [[ContactDetailMenuType]] = [[]]
  internal var model: Contact?
  private var type: ContactDetailType = .view
  
  private var firstName: String?
  private var lastName: String?
  private var phoneNumber: String?
  private var email: String?
  private var age: Int = 1
  
  init(model: Contact? = nil, type: ContactDetailType) {
    self.model = model
    self.type = type
    if type == .new {
      self.items = [[.firstName(detail: ""), .lastName(detail: ""),
                     .age(detail: ""),
                     .email(address: "")]]
    } else {
      fetchContact()
    }
  }
  
  func numberOfItemsInSection() -> Int {
    return items.count
  }
  
  func numberOfRowsInSection(section: Int) -> Int {
    return items[section].count
  }
  
  func items(cellForRowAt indexPath: IndexPath) -> ContactDetailMenuType {
    return items[indexPath.section][indexPath.row]
  }
  
  func getContact() -> Contact? {
    return model
  }
  
  func deleteContact() {
    delegate?.setupPage(with: .loading)
    guard let id = model?.id else { return }
    NetworkService.instance.requestObject(JenContactAPI.deleteContact(id: id), c: EmptyResponse.self) { [weak self] (resullt) in
      guard let `self` = self else { return }
      switch resullt {
      case .success:
        self.delegate?.setupPage(with: .routeBack)
      case .failure(let error):
        self.delegate?.setupPage(with: .error(error: error.message))
      }
    }
  }
  
  private func fetchContact() {
    delegate?.setupPage(with: .loading)
    guard let id = model?.id else { return }
    NetworkService.instance.requestObject(JenContactAPI.getDetailContact(id: "\(id)"), c: DetailContactResponse.self) { [weak self] (result) in
      guard let `self` = self else { return }
      switch result {
      case .success(let response):
        self.model = response.data
        switch self.type {
        case .view:
            self.items = [[.age(detail: "\(response.data.age)"),
                           .email(address: response.data.email ?? "")],
                          [.delete]]
          self.delegate?.setupPage(with: .success)
        case .edit:
          self.items = [[.firstName(detail: response.data.firstName ?? ""),
                         .lastName(detail: response.data.lastName ?? ""),
                         .age(detail: "\(response.data.age)"),
                         .email(address: response.data.email ?? "")]]
          self.delegate?.setupPage(with: .success)
        default: break
        }
      case .failure(let error):
        self.delegate?.setupPage(with: .error(error: error.message))
      }
    }
  }
  
  func updateContact() {
    if self.type == .edit {
      guard let id = model?.id else { return }
      delegate?.setupPage(with: .loading)
      NetworkService.instance
        .requestObject(JenContactAPI
          .putContact(id: "\(id)",
            firstName: firstName ?? model?.firstName ?? "",
            lastName: lastName ?? model?.lastName ?? "",
            photo: "N/A",
            age: age), c: DetailContactResponse.self) { [weak self] (result) in
        switch result {
        case .success:
          self?.delegate?.setupPage(with: .empty)
        case .failure(let error):
          self?.delegate?.setupPage(with: .error(error: error.message))
        }
      }
    } else {
      guard let firstName = firstName,
        let lastName = lastName else {
        return
      }
      delegate?.setupPage(with: .loading)
      NetworkService.instance
        .requestObject(JenContactAPI
          .postContact(firstName: firstName,
                       lastName: lastName,
                       photo: "N/A",
                       age: age),
                c: EmptyResponse.self) { [weak self] (result) in
          switch result {
          case .success:
            self?.delegate?.setupPage(with: .routeBack)
          case .failure(let error):
            self?.delegate?.setupPage(with: .error(error: error.message))
          }
      }
    }
  }
  
  func updateFavourite(action: Bool) {
    // Todo Something update action but API doesnt have
  }
  
  func setFirstName(with value: String?) {
    if let value = value {
      if value.count < 3 {
        delegate?.setupPage(with: .error(error: "First Name must be more than 3"))
      } else {
        firstName = value
      }
    }
  }
  
  func setLastName(with value: String?) {
    if let value = value {
      if value.count < 3 {
        delegate?.setupPage(with: .error(error: "Last Name must be more than 3"))
      } else {
        lastName = value
      }
    }
  }
  
  func setPhoneNumber(with value: String?) {
    phoneNumber = value
  }
  
  func setEmail(with value: String?) {
    email = value
  }
  
  func setAge(with value: String?) {
    let minimumValueAge: Int = 1
    let maximumValueAge: Int = 100
   
    if let value = value {
      let age = Int(value) ?? 1
      if age < minimumValueAge {
        delegate?.setupPage(with: .error(error: "Age is not allowed below 1"))
      } else if age > maximumValueAge {
        delegate?.setupPage(with: .error(error: "Age is not allowed more than 100"))
      } else {
        self.age = age
      }
    }
  }
  
  func setPhoto(with value: String) {
    // SEEMS API Does not supported yet
  }
}
