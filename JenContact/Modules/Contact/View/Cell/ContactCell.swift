//
//  ContactCell.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import UIKit
import Kingfisher

class ContactCell: UITableViewCell {
  
  @IBOutlet weak var contactPhotoView: UIImageView!
  @IBOutlet weak var contactNameLabel: UILabel!
  @IBOutlet weak var buttonFavourite: UIButton!
  
  
  override func layoutSubviews() {
    super.layoutSubviews()
    contactPhotoView.layer.cornerRadius = contactPhotoView.frame.size.height/2
    contactPhotoView.clipsToBounds = true
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    contactPhotoView.kf.indicatorType = .activity
    contactPhotoView.image = UIImage.init(named: "placeholder_photo")
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    contactPhotoView.image = nil
    contactNameLabel.text = ""
  }
}

extension ContactCell: IReusableCell {
  static var height: CGFloat {
    return 64.0
  }
  func configureCell(item: Contact) {
    contactNameLabel.text = item.fullName
    if let imageString = item.photo {
      if imageString == "N/A" {
        contactPhotoView.image = UIImage(named: "placeholder_photo")
      } else {
        contactPhotoView.kf.setImage(with: URL(string: imageString))
      }
    }
    buttonFavourite.isSelected = item.favorite ?? false
  }
}
