//
//  ContactController.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import Foundation
import UIKit

class ContactController: UIViewController, BaseViewType {
  
  // MARK: - Properties
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var searchBar: UISearchBar!
  
  private lazy var viewModel: ContactViewModel = {
    let viewModel = ContactViewModel()
    viewModel.delegate = self
    return viewModel
  }()
  
  private lazy var activityIndicator: UIActivityIndicatorView = {
    let activity = UIActivityIndicatorView(style: .medium)
    return activity
  }()
  
  // MARK: - Coordinator Functions
  var onContactTap: ((Contact, ContactDetailType) -> Void)?
  var onAddContactTap: (() -> Void)?
  
  // MARK: - ViewDidLoad
  override func viewDidLoad() {
    super.viewDidLoad()
    setupTableView()
    setupSearchBar()
    registerKeyboardNotifications()
    setupActivityIndicator()
  }
  
  // MARK: - ViewWillAppear
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setupNavigation()
    viewModel.fetchContact()
  }
  
  // MARK: - View Will Dissappear
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    navigationController?.navigationBar.configure(with: .transparent(translucent: true))
  }
  
  private func setupTableView() {
    tableView.delegate = self
    tableView.dataSource = self
    tableView.registerReusableCell(ContactCell.self)
  }
  
  private func setupSearchBar() {
    searchBar.delegate = self
  }
  
  private func deactive() {
    searchBar.endEditing(true)
    searchBar.resignFirstResponder()
  }
  
  private func setupNavigation() {
    navigationItem.title = "Contact"
    navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(handleAddContact(_:)))
    navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Groups", style: .plain, target: nil, action: nil)
    navigationController?.navigationBar.configure(with: .solid(color: UIColor.white))
  }
  
  private func registerKeyboardNotifications() {
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(keyboardWillShow(notification:)),
                                           name: UIResponder.keyboardWillShowNotification,
                                           object: nil)
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(keyboardWillHide(notification:)),
                                           name: UIResponder.keyboardWillHideNotification,
                                           object: nil)
  }
  
  private func setupActivityIndicator() {
    view.addSubview(activityIndicator)
    activityIndicator.frame = CGRect(x: UIScreen.main.bounds.midX,
                                     y: 0,
                                     width: 48, height: 48)
  }
  
  @objc
  private func keyboardWillShow(notification: NSNotification) {
    let userInfo: NSDictionary = notification.userInfo! as NSDictionary
    let keyboardInfo = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
    let keyboardSize = keyboardInfo.cgRectValue.size
    var contentInsets = tableView.contentInset
    contentInsets.bottom = keyboardSize.height
    tableView.contentInset = contentInsets
    tableView.scrollIndicatorInsets = contentInsets
  }
  
  @objc
  private func keyboardWillHide(notification: NSNotification) {
    var contentInsets = tableView.contentInset
    contentInsets.bottom = 0
    tableView.contentInset = contentInsets
    tableView.scrollIndicatorInsets = contentInsets
  }
  
  @objc
  private func handleAddContact(_ sender: UIBarButtonItem) {
    onAddContactTap?()
  }
}

extension ContactController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return ContactCell.height
  }
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if viewModel.numberOfItemContact(section: section) == 0 {
        return 0
    } else {
        return 28
    }
  }
  func sectionIndexTitles(for tableView: UITableView) -> [String]? {
    return viewModel.sectionIndexTitles()
  }
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    deactive()
    onContactTap?(viewModel.itemContactAt(indexPath), .view)
  }
}

extension ContactController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return viewModel.numberOfSectionContact()
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfItemContact(section: section)
  }
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(indexPath: indexPath) as ContactCell
    cell.configureCell(item: viewModel.itemContactAt(indexPath))
    return cell
  }
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return viewModel.titleForHeader(section: section)
  }
}

extension ContactController: PageDelegate {
  func setupPage(with state: PageState) {
    switch state {
    case .loading:
      DispatchQueue.main.async {
        self.activityIndicator.startAnimating()
      }
    case .success:
      DispatchQueue.main.async {
        self.tableView.reloadData()
        self.activityIndicator.stopAnimating()
      }
    case .error(let error):
      showAlertWith(title: "Error", message: error)
    default:
      break
    }
  }
}

extension ContactController: UISearchBarDelegate {
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    viewModel.filterContact(by: searchText)
  }
  
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    deactive()
  }
}
