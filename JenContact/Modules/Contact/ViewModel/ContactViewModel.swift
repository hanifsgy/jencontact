//
//  ContactViewModel.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import Foundation
import UIKit

protocol ContactViewModelType {
  func fetchContact()
  func numberOfItemContact(section: Int) -> Int
  func numberOfSectionContact() -> Int
  func titleForHeader(section: Int) -> String?
  func itemContactAt(_ indexPath: IndexPath) -> Contact
  func sectionIndexTitles() -> [String]?
  func filterContact(by name: String)
}

final class ContactViewModel: ContactViewModelType {
  weak var delegate: PageDelegate?
  internal var filteredContact: [ContactListSection] = []
  internal var contactListSection: [ContactListSection] = [] {
    didSet {
      filteredContact = contactListSection
    }
  }
  private var isSearching: Bool = false
  
  func fetchContact() {
    delegate?.setupPage(with: .loading)
    NetworkService.instance.requestObject(JenContactAPI.getContact, c: ContactResponse.self) { [weak self] (result) in
      guard let `self` = self else { return }
      switch result {
      case let .success(response):
        debugPrint("RESPONSE: \(response.data) count: \(response.data.count)")
        let sortedContact = response.data.sorted(by: { $0.fullName < $1.fullName })
        let sectionTitles = UILocalizedIndexedCollation.current().sectionTitles
        var calculationSections: [ContactListSection] = []
        for title in sectionTitles {
          let contact = sortedContact.filter({ $0.fullName.capitalized.hasPrefix(title) })
          let section = ContactListSection.init(sectionTitle: title, contacts: contact)
          calculationSections.append(section)
        }
        self.contactListSection = calculationSections
        self.delegate?.setupPage(with: .success)
      case .failure(let error):
        self.delegate?.setupPage(with: .error(error: error.message))
      }
    }
  }
  
  func numberOfItemContact(section: Int) -> Int {
    return filteredContact[section].contacts.count
  }
  
  func numberOfSectionContact() -> Int {
    return filteredContact.count
  }
  
  func titleForHeader(section: Int) -> String? {
    if filteredContact[section].contacts.count == 0 {
      return nil
    }
    return filteredContact[section].sectionTitle
  }
  
  func itemContactAt(_ indexPath: IndexPath) -> Contact {
    return filteredContact[indexPath.section].contacts[indexPath.row]
  }
  
  func sectionIndexTitles() -> [String]? {
    return isSearching ? nil : filteredContact.compactMap({ $0.sectionTitle })
  }
  
  func filterContact(by name: String) {
    self.isSearching = true
    let letter = contactListSection.filter { (section) -> Bool in
      return section.sectionTitle.lowercased().first == name.lowercased().first
    }
    /// Assume we don't need section titles A-Z,#, like many contact app
    /// Show section titles with "TOP NAME MATCHES". We just need 1 section to
    /// filter the results
    filteredContact = (name == "") ? contactListSection :
      [ContactListSection(sectionTitle: "TOP NAME MATCHES", contacts: letter[0].contacts.filter({ (contact) -> Bool in
      return contact.fullName.lowercased().contains(name.lowercased())
    }))]
    self.delegate?.setupPage(with: .success)
  }
}
