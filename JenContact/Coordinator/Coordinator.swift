//
//  Coordinator.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import UIKit

protocol Coordinator: AnyObject {
  var childCoordinator: [Coordinator] { get set }
  var navigationController: UINavigationController { get set }
  func start()
}

extension Coordinator {
  func add(child: Coordinator) {
    guard !childCoordinator.contains(where: { $0 === child }) else {
      return
    }
    childCoordinator.append(child)
  }
  
  func remove(child: Coordinator?) {
    guard let index = childCoordinator.firstIndex(where: { $0 === child }) else {
      return
    }
    childCoordinator.remove(at: index)
  }
  
  func removeAll() {
    childCoordinator.removeAll()
  }
}

protocol CoordinatorOutput {
  var finish: (() -> Void)? { get set }
}
