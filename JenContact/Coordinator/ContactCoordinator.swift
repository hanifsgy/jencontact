//
//  ContactCoordinator.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import UIKit

class ContactCoordinator: Coordinator, CoordinatorOutput {
  var childCoordinator: [Coordinator] = [Coordinator]()
  var navigationController: UINavigationController
  var finish: (() -> Void)?
  
  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
  }
  
  func start() {
    let viewController = ContactController()
    
    viewController.onContactTap = { [weak self] (contact, type) in
      DispatchQueue.main.async {
        let viewController = ContactDetailController()
        let viewModel = ContactDetailViewModel(model: contact, type: type)
        viewController.viewModel = viewModel
        viewController.pageType = type
        
        viewController.onBackPressed = { [weak self] in
          self?.navigationController.popViewController(animated: true)
        }
        
        viewController.onEditPressed = { [weak self] in
          DispatchQueue.main.async {
            let viewController = ContactEditController(style: .grouped)
            let viewModelEdit = ContactDetailViewModel(model: contact, type: .edit)
            viewController.viewModel = viewModelEdit
            viewController.pageType = .edit
            
            viewController.onCancelPressed = {
              self?.navigationController.popViewController(animated: true)
            }
            viewController.onDonePressed = {
              self?.navigationController.popToRootViewController(animated: true)
            }
            self?.navigationController.pushViewController(viewController, animated: true)
          }
        }
        self?.navigationController.pushViewController(viewController, animated: true)
      }
    }
    
    viewController.onAddContactTap = { [weak self] in
      DispatchQueue.main.async {
        let viewController = ContactEditController(style: .grouped)
        let viewModel = ContactDetailViewModel(model: nil, type: .new)
        viewController.viewModel = viewModel
        viewController.pageType = .new
        
        viewController.onCancelPressed = {
          self?.navigationController.popViewController(animated: true)
        }
        self?.navigationController.pushViewController(viewController, animated: true)
      }
    }
    
    navigationController.setViewControllers([viewController], animated: true)
  }
}
