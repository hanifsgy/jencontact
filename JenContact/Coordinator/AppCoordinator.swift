//
//  AppCoordinator.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {
  var childCoordinator: [Coordinator] = [Coordinator]()
  var navigationController: UINavigationController
  
  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
  }
  
  func start() {
    let coordinator = ContactCoordinator(navigationController: navigationController)
    coordinator.finish = { [weak self, weak coordinator] in
      self?.start()
      self?.remove(child: coordinator)
    }
    add(child: coordinator)
    coordinator.start()
  }
}
