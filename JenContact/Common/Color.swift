//
//  Color.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import UIKit

class Color: UIColor {
  public static func RGBColor(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
    return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
  }
  
  public static func RGBColor(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
    return RGBColor(red: red, green: green, blue: blue, alpha: 1)
  }
}

extension Color {
  static var primary_blue: UIColor {
    return Color.RGBColor(red: 2, green: 183, blue: 227)
  }
}
