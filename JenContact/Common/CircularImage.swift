//
//  CircularImage.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import Foundation
import UIKit

public class CircularUImage: UIImageView {
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  override public func prepareForInterfaceBuilder() {
    setup()
  }
  
  private func setup() {
    self.layer.cornerRadius = self.frame.width / 2
    self.clipsToBounds = true
  }
}
