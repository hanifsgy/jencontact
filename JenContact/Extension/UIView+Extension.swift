//
//  UIView+Extension.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import UIKit

extension UIView {
  func loadNib() -> UIView {
    let bundle = Bundle(for: type(of: self))
    let nibName = type(of: self).description().components(separatedBy: ".").last!
    let nib = UINib(nibName: nibName, bundle: bundle)
    return nib.instantiate(withOwner: self, options: nil).first as! UIView
  }
  func applyGradient(colors: [CGColor], location: [NSNumber]? = nil, position: CALayer.Position = .vertical) {
    let gradientLayer = CALayer.gradient(
      colors: colors,
      location: location,
      position: position,
      cornerRadius: self.layer.cornerRadius,
      bounds: self.bounds)
    self.layer.insertSublayer(gradientLayer, at: 0)
  }
}
