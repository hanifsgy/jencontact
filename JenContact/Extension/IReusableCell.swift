//
//  IReusableCell.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import UIKit

protocol IReusableCell {
  associatedtype DataType
  static var reuseIdentifier: String { get }
  static var nib: UINib? { get }
  func configureCell(item: DataType)
}

extension IReusableCell {
  static var reuseIdentifier: String {
    return String(describing: self)
  }
  
  static var nib: UINib? {
    guard let _ = Bundle.main.path(forResource: reuseIdentifier, ofType: "nib") else { return nil }
    return UINib(nibName: String(describing: self), bundle: nil)
  }
  
  func configureCell(item: Any) {
    
  }
}
