//
//  UITableView.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import UIKit

extension UITableView {
  func registerReusableCell<T: UITableViewCell>(_: T.Type) where T: IReusableCell {
    if let nib = T.nib {
      self.register(nib, forCellReuseIdentifier: T.reuseIdentifier)
    } else {
      self.register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    }
  }
  
  func registerReusableHeaderCell<T: UITableView>(_: T.Type) where T: IReusableCell {
    if let nib = T.nib {
      self.register(nib, forCellReuseIdentifier: T.reuseIdentifier)
    } else {
      self.register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    }
  }
  
  func dequeueReusableCell<T: UITableViewCell>(indexPath: IndexPath) -> T where T: IReusableCell {
    return self.dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as! T
  }
  
  func dequeueReusableCell<T: UITableViewCell>() -> T where T: IReusableCell {
    return self.dequeueReusableCell(withIdentifier: T.reuseIdentifier) as! T
  }
}
