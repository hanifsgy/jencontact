//
//  UINavigationBar.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import UIKit

extension UINavigationBar {
  enum NavigationBarKind {
    case transparent(translucent: Bool)
    case solid(color: UIColor)
  }
  
  func configure(with type: NavigationBarKind) {
    switch type {
    case .transparent(let translucent):
      setBackgroundImage(UIImage(), for: .default)
      shadowImage = UIImage()
      isTranslucent = translucent
      tintColor = Color.primary_blue
      titleTextAttributes = [
        NSAttributedString.Key(
          rawValue: NSAttributedString.Key.foregroundColor.rawValue
        ): UIColor.white
      ]
      barTintColor = UIColor.clear
      backgroundColor = UIColor.clear
    case .solid(let color):
      shadowImage = UIImage()
      isTranslucent = false
      tintColor = Color.primary_blue
      titleTextAttributes = [
        NSAttributedString.Key(
          rawValue: NSAttributedString.Key.foregroundColor.rawValue
        ): UIColor.black
      ]
      barTintColor = color
      backgroundColor = color
    }
  }
}
