//
//  JenContactAPI.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import Foundation
import Moya

enum JenContactAPI {
  case getContact
  case getDetailContact(id: String)
  case postContact(firstName: String, lastName: String, photo: String, age: Int)
  case putContact(id: String, firstName: String, lastName: String, photo: String, age: Int)
  case deleteContact(id: String)
}

extension JenContactAPI: TargetType {
  var baseURL: URL {
    return URL(string: "https://simple-contact-crud.herokuapp.com")!
  }
  
  var path: String {
    switch self {
    case .getContact, .postContact:
      return "/contact"
    case .getDetailContact(let id),
         .putContact(let (id,_,_,_,_)),
         .deleteContact(let id):
      return "/contact/\(id)"
    }
  }
  
  var method: Moya.Method {
    switch self {
    case .getContact, .getDetailContact:
      return .get
    case .postContact:
      return .post
    case .putContact:
      return .put
    case .deleteContact:
      return .delete
    }
  }
  
  var sampleData: Data {
    switch self {
    case .getContact:
      return getSampleJSONAsData(with: "Contacts.json")
    case .getDetailContact:
      return getSampleJSONAsData(with: "DetailContact.json")
    default:
      return Data()
    }
  }
  
  var task: Task {
    switch self {
    case .postContact, .putContact:
      return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
    default:
      return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }
  }
  
  var headers: [String : String]? {
    return nil
  }
  
  var parameters: [String: Any] {
    switch self {
    case .postContact(let (firstName, lastName, photo, age)):
      return [
        "firstName": firstName,
        "lastName": lastName,
        "photo": photo,
        "age": age
      ]
    case .putContact(let (_, firstName, lastName, photo, age)):
      return [
        "firstName": firstName,
        "lastName": lastName,
        "photo": photo,
        "age": age
      ]
    default:
      return [:]
    }
  }
}

extension JenContactAPI {
  private func getSampleJSONAsData(with name: String) -> Data {
    guard let path = Bundle.main.path(forResource: name, ofType: "json") else { return Data() }
    do {
      let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
      return data
    } catch {
      return Data()
    }
  }
}
