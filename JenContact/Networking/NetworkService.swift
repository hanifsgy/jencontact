//
//  NetworkService.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import Foundation
import Moya

struct NetworkService {
  public static let instance = NetworkService()
  fileprivate let provider: MoyaProvider<MultiTarget>
  
  private init() {
    self.provider = MoyaProvider<MultiTarget>(
//      stubClosure: stubClosure
    )
  }
  
  private let stubClosure = { (target: MultiTarget) -> StubBehavior in
    #if DEBUG
    return .immediate
    #else
    return .never
    #endif
  }
}

extension NetworkService {
  func requestObject<T: TargetType, C: Decodable>(_ t: T, c: C.Type, completion: @escaping (Result<C, NetworkError>) -> Void) {
    provider.request(MultiTarget(t)) { (result) in
      switch result {
      case .success(let value):
        do {
          let filteredResponse = try value.filterSuccessfulStatusCodes()
          let result = try filteredResponse.map(C.self)
          completion(.success(result))
        } catch let DecodingError.dataCorrupted(context) {
          print(context)
          completion(.failure(NetworkError.IncorrectDataReturned))
        } catch let DecodingError.keyNotFound(key, context) {
          print("Key '\(key)' not found:", context.debugDescription)
          print("codingPath:", context.codingPath)
          completion(.failure(NetworkError.IncorrectDataReturned))
        } catch let DecodingError.valueNotFound(value, context) {
          print("Value '\(value)' not found:", context.debugDescription)
          print("codingPath:", context.codingPath)
          completion(.failure(NetworkError.IncorrectDataReturned))
        } catch let DecodingError.typeMismatch(type, context)  {
          print("Type '\(type)' mismatch:", context.debugDescription)
          print("codingPath:", context.codingPath)
          completion(.failure(NetworkError.IncorrectDataReturned))
        } catch {
          do {
            completion(.failure(NetworkError.SoftError(message: "Error")))
          }
        }
      case .failure(let error):
        switch error {
        case .underlying(let (error, _)):
          completion(.failure(NetworkError(error: error as NSError)))
        default:
          completion(.failure(NetworkError.IncorrectDataReturned))
        }
      }
    }
  }
}

