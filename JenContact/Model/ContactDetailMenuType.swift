//
//  ContactDetailMenuType.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import Foundation
import UIKit

enum ContactDetailMenuType {
  case age(detail: String)
  case email(address: String)
  case delete
  case firstName(detail: String)
  case lastName(detail: String)
  
  var description: String? {
    switch self {
    case .age:
      return "age"
    case .email:
      return "email"
    case .delete:
      return "Delete"
    case .firstName:
      return "First Name"
    case .lastName:
      return "Last Name"
    }
  }
  
  var content: String? {
    switch self {
    case .age(let phone):
      return phone
    case .email(let address):
      return address
    case .firstName(let detail):
      return detail
    case .lastName(let detail):
      return detail
    default:
      return nil
    }
  }
  
  var keyboardType: UIKeyboardType {
    switch self {
    case .age:
      return .numberPad
    case .email:
      return .emailAddress
    default:
      return .default
    }
  }
}

enum ContactDetailType {
  case edit
  case view
  case new
}

protocol ContactPageDelegate: class {
  func setupPage(with page: ContactDetailType)
}
