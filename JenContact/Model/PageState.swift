//
//  PageState.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import Foundation

enum PageState {
  case loading
  case empty
  case error(error: String)
  case success
  case routeBack
}
