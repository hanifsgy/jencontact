//
//  Contact.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import Foundation


struct ContactResponse: Codable {
    let message: String?
    let data: [Contact]
}

struct DetailContactResponse: Codable {
    let message: String?
    let data: Contact
}

struct EmptyResponse: Codable {
  let message: String?
}

struct Contact: Codable {
  let id: String
  let firstName: String?
  let lastName: String?
  let photo: String?
  let favorite: Bool?
  let url: String?
  let phoneNumber: String?
  let email: String?
  let age: Int
        
  var fullName: String {
    if firstName == nil && lastName == nil {
      return ""
    } else if firstName == nil {
      return lastName ?? ""
    } else if lastName == nil {
      return firstName ?? ""
    } else {
      return (firstName ?? "")  + " " + (lastName ?? "")
    }
  }
  
  private enum CodingKeys: String, CodingKey {
    case id, email, url, favorite, age, photo
    case firstName = "firstName"
    case lastName = "lastName"
    case phoneNumber = "phone_number"
  }
  
}
