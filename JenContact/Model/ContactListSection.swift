//
//  ContactListSection.swift
//  JenContact
//
//  Created by Hanif Sugiyanto on 18/06/20.
//  Copyright © 2020 Personal Organization. All rights reserved.
//

import Foundation

struct ContactListSection {
  let sectionTitle: String
  let contacts: [Contact]
}
